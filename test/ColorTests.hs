module ColorTests
  ( colorTests
  ) where

import Test.Tasty
import Test.Tasty.HUnit

import Color

colorTests :: TestTree
colorTests = testGroup "Color tests" [rgbFromStringTests]

rgbFromStringTests :: TestTree
rgbFromStringTests = testGroup "rgbFromString tests"
  [ testCase "String with hash (black)" $
      rgbFromString "#000000" @?= RGB 0 0 0

  , testCase "String with hash (white)" $
      rgbFromString "#ffffff" @?= RGB 255 255 255

  , testCase "String without hash" $
      rgbFromString "E67E22" @?= RGB 0xe6 0x7e 0x22

  , testCase "Invalid color string results in magenta" $
      rgbFromString "hello" @?= RGB 255 0 255
  ]
