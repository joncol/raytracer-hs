module LinAlgTests where

import Test.Tasty
import Test.Tasty.HUnit

import LinAlg

linAlgTests :: TestTree
linAlgTests = testGroup "LinAlg tests" [majorAxisTests]

majorAxisTests :: TestTree
majorAxisTests = testGroup "majorAxis tests"
  [ testCase "Returns the major axis of a vector" $
      majorAxis (V3 1 2 3) @?= Z

  , testCase "Works for negative coordinates" $
      majorAxis (V3 1 (-3) 2) @?= Y

  , testCase "Returns first axis when coordinates are equal" $
      majorAxis (V3 0 0 0) @?= X
  ]
