import Test.Tasty
import Test.Tasty.HUnit

import ColorTests
import LinAlgTests

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [unitTests]

unitTests :: TestTree
unitTests = testGroup "Unit tests" [colorTests, linAlgTests]
