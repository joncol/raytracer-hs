## Ray Tracer

A ray tracer written in Haskell with support for area lights (simulated using
multiple point lights). Uses the
[repa](https://hackage.haskell.org/package/repa) package to parallelize ray
tracing across multiple cores.

Example renders:

![Sphere flake](progress_pics/41-flake.png)

![Soft shadows](progress_pics/36-nice.png)
