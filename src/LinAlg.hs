{-# LANGUAGE Rank2Types #-}

module LinAlg where

import Control.Lens.Type
import Control.Monad
import Linear.Metric
import Linear.V2
import Linear.V3
import Linear.Vector
import System.Random.MWC

data Ray = Ray { rayOrigin :: !(V3 Double)
               , rayDir    :: !(V3 Double)
               } deriving Show

rayPoint :: Ray -> Double -> (V3 Double)
{-# INLINE rayPoint #-}
rayPoint (Ray o d) t = o ^+^ t *^ d

rayOffset :: Ray -> Double -> Ray
{-# INLINE rayOffset #-}
rayOffset ray@(Ray _ d) t = Ray (rayPoint ray t) d

reflDirection :: Num a => V3 a -> V3 a -> V3 a
{-# INLINE reflDirection #-}
reflDirection i n = (-2*i `dot` n *^ n) ^+^ i

halfAngleDir :: V3 Double -> V3 Double -> V3 Double
{-# INLINE halfAngleDir #-}
halfAngleDir u v = normalize $ u ^+^ v

tranDirection :: Double    -- ^ Relative refraction index.
              -> V3 Double -- ^ Incident vector (assumed normalized).
              -> V3 Double -- ^ Normal.
              -> Maybe (V3 Double)
tranDirection eta i n
  | d < 0     = Nothing
  | otherwise = Just $ (eta * ci - sqrt d) *^ n ^+^ eta *^ i
  where
    ci = -n `dot` i
    d  = 1 - eta*eta*(1 - ci*ci)

newtype Grid = Grid [(Double, Double)] deriving (Eq, Show)

-- | Calculate a regular (n x n) 2D grid having a given starting point and a
-- | size.
regular2DGrid :: Int    -- ^ The grid size.
              -> Double -- ^ The x offset of the output coordinates.
              -> Double -- ^ The y offset of the output coordinates.
              -> Double -- ^ The width of the resulting grid.
              -> Double -- ^ The height of the resulting grid.
              -> Grid   -- ^ The resulting grid.
{-# INLINE regular2DGrid #-}
regular2DGrid n x y w h =
  Grid $ map (\(V2 cx cy) -> (x + w * (fromIntegral cx + 0.5) / n',
                              y + h * (fromIntegral cy + 0.5) / n')) ps
  where
    ps = flip V2 <$> [0..n-1] <*> [0..n-1]
    n' = fromIntegral n

-- | Calculate a random (n x n) 2D grid having a given starting point and a
-- | size.
random2DGrid :: Int     -- ^ The grid size.
             -> Double  -- ^ The x offset of the output coordinates.
             -> Double  -- ^ The y offset of the output coordinates.
             -> Double  -- ^ The width of the resulting grid.
             -> Double  -- ^ The height of the resulting grid.
             -> GenIO   -- ^ Random number generator
             -> IO Grid -- ^ The resulting grid.
{-# INLINE random2DGrid #-}
random2DGrid n x y w h gen =
  Grid <$> (forM ps $ \(V2 cx cy) -> do
    rx <- uniformR (0, 1) gen
    ry <- uniformR (0, 1) gen
    return (x + w * (fromIntegral cx + rx) / n',
            y + h * (fromIntegral cy + ry) / n'))
  where
    ps = flip V2 <$> [0..n-1] <*> [0..n-1]
    n' = fromIntegral n

data Axis = AxisX | AxisY | AxisZ deriving (Eq, Show)

majorAxis :: V3 Double -> Axis
{-# INLINE majorAxis #-}
majorAxis (V3 x y z)
  | abs x >= abs y && abs x >= abs z = AxisX
  | abs y >= abs z                   = AxisY
  | otherwise                        = AxisZ

project :: R3 t => Axis -> Lens' (t a) (V2 a)
{-# INLINE project #-}
project AxisX = _yz
project AxisY = _xz
project AxisZ = _xy
