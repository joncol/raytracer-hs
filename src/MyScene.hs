{-# OPTIONS_GHC -fno-warn-missing-signatures
                -fno-warn-name-shadowing
                -fno-warn-unused-imports #-}

module MyScene where

import           Control.Monad.Random
import qualified Data.Colour.RGBSpace as C
import qualified Data.Colour.RGBSpace.HSV as C
import qualified Data.Colour.RGBSpace.HSL as C
import           Linear.Metric
import           Linear.V1
import           Linear.V2
import           Linear.V3
import           Linear.Vector
import           System.Random.MWC
import           Data.Tree (Tree)
import qualified Data.Tree as T

import           AABB
import           AreaLight
import           Camera
import           Color
import           LinAlg
import           Plane
import           PointLight
import           Polygon
import           Rectangle
import           Scene
import           Shapes
import           SolidGen
import           Sphere
import           Surface

scene1 w h = withSystemRandom $ \gen -> do
  grid1 <- randomGridFromRect shadowGridN areaLight1 gen
  grid2 <- randomGridFromRect shadowGridN areaLight2 gen
  grid3 <- randomGridFromRect shadowGridN areaLight3 gen
  return Scene
    { scWidth  = w
    , scHeight = h
    -- , scCamera = Camera { camPos = V3 (-10) 15 10
    --                     , camDir = V3 5 (-12) (-35)
    , scCamera = Camera { camPos = V3 0 0 0
                        , camDir = V3 0 0 (-1)
                        , camUp  = V3 0 1 0
                        , camFov = 60
                        }
    , scSolids        = map AnySolid (sphereRingOuter 64 surfGen1 ++
                                      sphereRingInner 32 surfGen2) ++
                        map AnySolid [plane1, plane2]
    , scPointLights   = []
    , scAreaLights    = [ AreaLight areaLight1 grid1
                        , AreaLight areaLight2 grid2
                        , AreaLight areaLight3 grid3
                        ]
    , scAmbientColor  = colorFromString "#050e20"
    , scMaxLevel      = 2
    , scSuperSampling = 2
    , scShadowGridN   = shadowGridN
    , scMinWeight     = 0.01
    }
  where
    shadowGridN = 8
    plane1 = Plane (normalize $ V3 0 1 0) 13.5 planeSurf1
    planeSurf1 = Surface
      { surfColor     = colorFromString "#6c1a9c"
      , surfDiff      = 0.9
      , surfSpec      = 0.6
      , surfShininess = 20
      , surfRefl      = 0.85
      , surfTran      = 0
      , surfRefrIndex = 1
      }
    plane2 = Plane (normalize $ V3 1 0 8) 70 planeSurf2
    planeSurf2 = Surface
      { surfColor     = colorFromString "#eceaec"
      , surfDiff      = 0.4
      , surfSpec      = 0.3
      , surfShininess = 10
      , surfRefl      = 0.45
      , surfTran      = 0
      , surfRefrIndex = 1
      }
    sphereRingOuter n fSurf =
        evalRand (sphereGen n (0.8, 2.5) fx fy fz fSurf) (mkStdGen 1)
      where a t  = 2 * pi * t
            fx t = 12 * cos (a t)
            fy t = 12 * sin (a t)
            fz t = (-35) + 10 * sin (6 * a t)

    sphereRingInner n fSurf =
        evalRand (sphereGen n (0.6, 1.5) fx fy fz fSurf) (mkStdGen 1)
      where a t  = 2 * pi * t
            fx t = 4 * cos (a t)
            fy t = 4 * sin (a t)
            fz t = (-35) + 5 * sin (9 * a t)

    -- light1 = PointLight (V3 7.4 10 0) white
    -- light2 = PointLight (V3 (-10) (-5) (-10)) (colorFromString "#e940f9")
    -- light3 = PointLight (V3 40 20 (-2)) (colorFromString "#80faa0")

    areaLight1 = createRect (V3 7.4 10 0)
                            (V3 0 0 (-30)) 5
                            (def { surfColor = white })

    areaLight2 = createRect (V3 (-10) (-5) (-10))
                            (V3 0 0 (-30)) 5
                            (def { surfColor = colorFromString "#e940f9" })

    areaLight3 = createRect (V3 40 20 (-2))
                            (V3 0 0 (-30)) 5
                            (def { surfColor = colorFromString "#80faa0" })


surfGen1 :: RandomGen g => Double -> Rand g Surface
surfGen1 t =
  do
    kd     <- getRandomR (0.2, 0.85)
    ks     <- getRandomR (0.1, 0.95)
    kshine <- getRandomR (1, 150)
    kr     <- getRandomR (0.2, 0.9)
    let V1 h = lerp t (V1 360) (V1 180)
    let s = 0.9
    let v = 0.5
    let C.RGB r g b = C.hsl h s v

    return Surface
      { surfColor     = Color r g b
      , surfDiff      = kd
      , surfSpec      = ks
      , surfShininess = kshine
      , surfRefl      = kr
      , surfTran      = 0
      , surfRefrIndex = 1
      }

surfGen2 :: RandomGen g => Double -> Rand g Surface
surfGen2 t =
  do
    kd     <- getRandomR (0.4, 0.95)
    ks     <- getRandomR (0.1, 0.75)
    kshine <- getRandomR (1, 50)
    kr     <- getRandomR (0.4, 0.95)
    s      <- getRandomR (0.5, 0.95)
    let V1 h = lerp t (V1 120) (V1 0)
    let v = 0.5
    let C.RGB r g b = C.hsl h s v

    return Surface
      { surfColor     = Color r g b
      , surfDiff      = kd
      , surfSpec      = ks
      , surfShininess = kshine
      , surfRefl      = kr
      , surfTran      = 0
      , surfRefrIndex = 1
      }

simpleScene w h = withSystemRandom $ \gen -> do
  grid1 <- randomGridFromRect shadowGridN areaLight1 gen
  grid2 <- randomGridFromRect shadowGridN areaLight2 gen
  return Scene
    { scWidth         = w
    , scHeight        = h
    , scCamera        = cameraLookAt (V3 0 (25) 0)
                                     (V3 0 10 (-25))
    , scSolids        = [ --AnySolid sphere1
                          AnySolid sphere2
                        -- , AnySolid plane1
                        , AnySolid box1
                        ]
    , scPointLights   = []
    , scAreaLights    = [AreaLight areaLight1 grid1, AreaLight areaLight2 grid2]
    , scAmbientColor  = colorFromString "#010210"
    , scMaxLevel      = 4
    , scSuperSampling = 1
    , scShadowGridN   = shadowGridN
    , scMinWeight     = 0.01
    }
  where
    shadowGridN = 1
    sphere1 = Sphere (V3 (-12) 1 (-30)) 7 sphereSurf1
    sphereSurf1 = Surface
      { surfColor     = colorFromString "#ffc73d"
      , surfDiff      = 0.8
      , surfSpec      = 0.6
      , surfShininess = 100
      , surfRefl      = 0.9
      , surfTran      = 0
      , surfRefrIndex = 1
      }

    sphere2 = Sphere (V3 0 10 (-25)) 5 sphereSurf2
    sphereSurf2 = Surface
      { surfColor     = colorFromString "#e73df0"
      , surfDiff      = 0.7
      , surfSpec      = 0.8
      , surfShininess = 120
      , surfRefl      = 0.0
      , surfTran      = 0.5
      , surfRefrIndex = 1
      }

    plane1 = Plane (normalize $ V3 0 1 0) 6 planeSurf1
    planeSurf1 = Surface
      { surfColor     = colorFromString "#8c5a2c"
      , surfDiff      = 0.9
      , surfSpec      = 0.6
      , surfShininess = 20
      , surfRefl      = 0.85
      , surfTran      = 0
      , surfRefrIndex = 1
      }

    box1 = AABB (V3 (-5) (0) (-20)) (V3 5 (10) (-30)) boxSurf1
    boxSurf1 = Surface
      { surfColor     = colorFromString "#80c4e0"
      , surfDiff      = 0.9
      , surfSpec      = 0.6
      , surfShininess = 20
      , surfRefl      = 0.95
      , surfTran      = 0.8
      , surfRefrIndex = 1
      }

    areaLight1 = Rectangle
      { rectCenter  = V3 0 20 (-45)
      , rectRight   = V3 1 0 0
      , rectUp      = V3 0 1 1
      , rectWidth   = 20
      , rectHeight  = 20
      , rectSurface = def { surfColor = colorFromString "#e940f9" }
      }

    areaLight2 = createRect (V3 (-10) (15) (0))
                            (V3 0 0 (-20)) 5
                            (def { surfColor = colorFromString "#e940f9" })

surf3 = Surface
  { surfColor     = colorFromString "#e74c3c"
  , surfDiff      = 1
  , surfSpec      = 0.6
  , surfShininess = 20
  , surfRefl      = 0.5
  , surfTran      = 0
  , surfRefrIndex = 1
  }

sphere3 = Sphere (V3 (-10) 5 (-20)) 2 surf3


sphereFlakeScene w h = withSystemRandom $ \gen -> do
  grid1 <- randomGridFromRect shadowGridN areaLight1 gen
  grid2 <- randomGridFromRect shadowGridN areaLight2 gen
  grid3 <- randomGridFromRect shadowGridN areaLight3 gen
  grid4 <- randomGridFromRect shadowGridN areaLight4 gen

  let spheres = T.flatten $ sphereFlake (V3 0 0 (-30)) 5.5 0.4 3 sphereSurf

  -- putStrLn $ "Sphere count: " ++ (show . length $ spheres)

  return Scene
    { scWidth       = w
    , scHeight      = h
    , scCamera      = cameraLookAt (V3 (-10) (15) 0)
                                   (V3 (-4) 5 (-20))
    , scSolids      = map AnySolid spheres ++
                      map AnySolid [plane1, plane2, plane3]
    , scPointLights = []
    , scAreaLights  = map (uncurry AreaLight) [ (areaLight1, grid1)
                                              , (areaLight2, grid2)
                                              , (areaLight3, grid3)
                                              , (areaLight4, grid4)
                                              ]
    , scAmbientColor  = colorFromString "#020308"
    , scMaxLevel      = 2
    , scSuperSampling = 1
    , scShadowGridN   = shadowGridN
    , scMinWeight     = 0.01
    }
  where
    shadowGridN = 1
    sphereSurf = Surface
      { surfColor     = colorFromString "#c73d00"
      , surfDiff      = 0.5
      , surfSpec      = 0.7
      , surfShininess = 120
      , surfRefl      = 0.9
      , surfTran      = 0
      , surfRefrIndex = 1
      }

    plane1 = Plane (normalize $ V3 0 1 0) 13 planeSurf1
    planeSurf1 = Surface
      { surfColor     = colorFromString "#401008"
      , surfDiff      = 0.5
      , surfSpec      = 0.1
      , surfShininess = 20
      , surfRefl      = 0.95
      , surfTran      = 0
      , surfRefrIndex = 1
      }

    plane2 = Plane (normalize $ V3 1 0 1) 36 planeSurf2
    planeSurf2 = Surface
      { surfColor     = colorFromString "#7f8c8d"
      , surfDiff      = 0.6
      , surfSpec      = 0.1
      , surfShininess = 20
      , surfRefl      = 0.95
      , surfTran      = 0
      , surfRefrIndex = 1
      }

    plane3 = Plane (normalize $ V3 (-1) 0 1) 50 planeSurf3
    planeSurf3 = Surface
      { surfColor     = colorFromString "#2c3e50"
      , surfDiff      = 0.70
      , surfSpec      = 0.5
      , surfShininess = 20
      , surfRefl      = 0.99
      , surfTran      = 0
      , surfRefrIndex = 1
      }

    areaLight1 = createRect (V3 7.4 30 0)
                            (V3 0 0 (-30)) 5
                            (def { surfColor = white })

    areaLight2 = createRect (V3 (-20) (-1) (-10))
                            (V3 0 0 (-30)) 5
                            (def { surfColor = colorFromString "#e940f9" })

    areaLight3 = createRect (V3 40 40 (-2))
                            (V3 0 0 (-30)) 5
                            (def { surfColor = colorFromString "#80faa0" })

    areaLight4 = createRect (V3 (-10) 20 (-30))
                            (V3 0 0 (-30)) 5
                            (def { surfColor = colorFromString "#edf1a7" })
