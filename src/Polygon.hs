{-# LANGUAGE NamedFieldPuns, RecordWildCards #-}

module Polygon
  ( Polygon (..)
  ) where

import Control.Lens
import Data.Bool
import Linear.V2
import Linear.V3

import Intersection
import LinAlg
import Plane
import Shapes
import Surface
data Polygon = Polygon
  { polyVertices :: [V3 Double]
  , polySurface  :: Surface
  } deriving Show

instance Solid Polygon where
  intersect r (Polygon vs@(v0:v1:v2:_) _) =
    case r `intersect` plane of
      []    -> []
      i : _ -> bool [] [i] $ polyIntersect plane vs (rayPoint r $ isectT i)
    where plane = planeFromPoints v0 v1 v2

  intersect _ _ = undefined

instance Primitive Polygon where
  surface Polygon { polySurface } = polySurface

polyIntersect :: Plane -> [V3 Double] -> V3 Double -> Bool
polyIntersect Plane {..} vs p =
    polyIntersect2D (map (^.proj) vs) (p ^. proj)
  where proj = project (majorAxis planeNormal)

-- Uses Jordan's curve theorem to find if a 2D point is inside a 2D polygon.
polyIntersect2D :: [V2 Double] -> V2 Double -> Bool
polyIntersect2D vs (V2 px py) = odd $ numCrossings vs'
  where vs' = map (\(V2 x y) -> V2 (x - px) (y - py)) vs

-- Calculates the number of times the positive X-axis is crossed by any edge.
numCrossings :: [V2 Double] -> Int
numCrossings vs = length . filter (==True) . map edgeCrossesPosXAxis $ edges
  where edges = zip vs (rotate 1 vs)

rotate :: Int -> [a] -> [a]
rotate _ [] = []
rotate n xs = zipWith const (drop n $ cycle xs) xs

edgeCrossesPosXAxis :: (V2 Double, V2 Double) -> Bool
edgeCrossesPosXAxis (V2 x0 y0, V2 x1 y1)
  | signum y0 == signum y1 = False
  | x0 < 0 && x1 < 0       = False
  | x0 > 0 && x1 > 0       = True
  | otherwise              = x0 - y0 * (x1 - x0) / (y1 - y0) > 0
