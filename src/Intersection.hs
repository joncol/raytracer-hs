module Intersection where

import Linear.V3
import Surface

-- Intersection. Primitive type a.
data Intersection a = Intersection
  { isectT      :: Double
  , isectNormal :: V3 Double
  , isectPrim   :: a
  , isectEnter  :: Bool          -- Entering solid material.
  , isectMedium :: Maybe Surface -- Material we're in just before intersection.
  } deriving Show
