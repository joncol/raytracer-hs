{-# LANGUAGE NamedFieldPuns #-}

module AreaLight where

import LinAlg
import Rectangle

data AreaLight = AreaLight
  { areaLightRect       :: Rectangle
  , areaLightRandomGrid :: Grid
  } deriving Show
