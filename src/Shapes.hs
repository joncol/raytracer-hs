{-# LANGUAGE ExistentialQuantification #-}

module Shapes where

import Intersection
import LinAlg
import Surface

class Show a => Solid a where
  intersect :: Ray -> a -> [Intersection AnyPrimitive]

data AnySolid = forall a. Solid a => AnySolid a

instance Show AnySolid where
  show (AnySolid a) = show a

-- A Solid is either a Primitive object or a CsgNode.
instance Solid AnySolid where
  {-# INLINE intersect #-}
  ray `intersect` (AnySolid a) = ray `intersect` a

class Solid a => Primitive a where
  surface :: a -> Surface

data AnyPrimitive = forall a. Primitive a => AnyPrimitive a

instance Show AnyPrimitive where
  show (AnyPrimitive a) = show a

instance Solid AnyPrimitive where
  {-# INLINE intersect #-}
  ray `intersect` (AnyPrimitive a) = ray `intersect` a
