{-# LANGUAGE NamedFieldPuns #-}

module Rectangle where

import Linear.Metric
import Linear.V3
import Linear.Vector
import System.Random.MWC

import LinAlg
import Polygon
import Shapes
import Surface

data Rectangle = Rectangle
  { rectCenter  :: V3 Double
  , rectRight   :: V3 Double -- The horizontal axis of the rectangle.
  , rectUp      :: V3 Double -- The vertical axis of the rectangle.
  , rectWidth   :: Double
  , rectHeight  :: Double
  , rectSurface :: Surface
  } deriving Show

instance Solid Rectangle where
  ray `intersect` (Rectangle c r u w h _) = ray `intersect` poly
    where
      u'   = normalize u
      r'   = normalize r
      wv   = (w/2) *^ r'
      hv   = (h/2) *^ u'
      vs   = [ c ^-^ wv ^-^ hv
             , c ^+^ wv ^-^ hv
             , c ^+^ wv ^+^ hv
             , c ^-^ wv ^+^ hv
             ]
      poly = Polygon vs def

instance Primitive Rectangle where
  surface Rectangle { rectSurface } = rectSurface

createRect :: V3 Double -> V3 Double -> Double -> Surface -> Rectangle
createRect p at size = Rectangle p u v size size
  where
    n = normalize (at ^-^ p)
    u | majorAxis n == AxisX = V3 0 (-nz) ny
      | majorAxis n == AxisY = V3 (-nz) 0 nx
      | otherwise            = V3 (-ny) nx 0
      where V3 nx ny nz = n
    v = n `cross` u

randomGridFromRect :: Int -> Rectangle -> GenIO -> IO Grid
randomGridFromRect n rect gen = random2DGrid n (-w/2) (-h/2) w h gen
  where
    w = rectWidth rect
    h = rectHeight rect
