{-# LANGUAGE NamedFieldPuns #-}

module Sphere
  ( Sphere (..)
  , containedIn
  ) where

import Data.Bool
import Linear.Metric
import Linear.V3
import Linear.Vector

import Intersection
import LinAlg
import Math (epsilon)
import Shapes
import Surface

data Sphere = Sphere { sphereCenter  :: V3 Double
                     , sphereRadius  :: Double
                     , sphereSurface :: Surface
                     } deriving Show

instance Solid Sphere where
  -- Assumes direction of ray is normalized.
  intersect ray@(Ray o d) sphere@(Sphere c r surf)
    | not inside && tca < 0 = []
    | t2hc < 0              = []
    | not inside            = [ isect (tca - sqrt t2hc) True
                              , isect (tca + sqrt t2hc) False
                              ]
    | otherwise             = [isect (tca + sqrt t2hc) False]
    where
      sr2         = r * r
      oc          = c ^-^ o
      l2oc        = oc `dot` oc
      inside      = l2oc < sr2
      tca         = oc `dot` d
      t2hc        = sr2 - l2oc + tca * tca
      normal t    = (rayPoint ray t ^-^ c) ^/ r
      isect t ent = Intersection { isectT      = t
                                 , isectNormal = normal t
                                 , isectPrim   = AnyPrimitive sphere
                                 , isectEnter  = ent
                                 , isectMedium = bool Nothing (Just surf) inside
                                 }

instance Primitive Sphere where
  surface Sphere { sphereSurface } = sphereSurface

containedIn :: Sphere -> Sphere -> Bool
Sphere ci ri _ `containedIn` Sphere co ro _ =
  norm (co ^-^ ci) + ri < ro + epsilon
