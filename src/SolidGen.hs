module SolidGen where

import           Control.Monad.Random
import           Data.List (zip4)
import           Data.Tree (Tree)
import qualified Data.Tree as T
import           Linear.V3
import           Linear.Vector

import           Sphere
import           Surface

sphereGen :: RandomGen g
          => Int
          -> (Double, Double)
          -> (Double -> Double)
          -> (Double -> Double)
          -> (Double -> Double)
          -> (Double -> Rand g Surface)
          -> Rand g [Sphere]
sphereGen n (minR, maxR) fx fy fz fSurf =
  traverse (\t -> getRandomR (minR, maxR) >>= mkSphere t) ts
  where
    ts           = map (\x -> fromIntegral x / fromIntegral n) [0..n - 1]
    mkSphere t r = Sphere (V3 (fx t) (fy t) (fz t)) r <$> fSurf t

sphereFlake :: V3 Double -- ^ Center of sphere flake
            -> Double    -- ^ Radius of sphere flake
            -> Double    -- ^ Radius factor
            -> Int       -- ^ Max recursive depth
            -> Surface
            -> Tree Sphere

sphereFlake center radius radiusMult maxDepth surf =
  T.unfoldTree builder (center, radius, [], 0)

  where
    builder :: (V3 Double, Double, [Sphere], Int)
            -> (Sphere, [(V3 Double, Double, [Sphere], Int)])
    builder (c, r, ancestors, lvl) = (sphere, seed)
      where
        sphere = Sphere c r surf
        rng    = [0..3] :: [Int]
        thetas = map ((*(pi/2)) . fromIntegral) rng
        angles = (0, 0) : (0, pi) : map (flip (,) (pi/2)) thetas
        r'     = r * radiusMult

        seed
          | lvl >= maxDepth = []
          | otherwise       = zip4 (filter p $ map getCenter angles)
                                   (repeat r')
                                   (repeat $ sphere : ancestors)
                                   (repeat $ lvl+1)

        getCenter (theta, phi) = c ^+^ (d *^ sp)
          where
            d  = r + r'
            sp = V3 (sin phi * cos theta)
                    (sin phi * sin theta)
                    (cos phi)

        p c' = case ancestors of
                 []     -> True
                 gp : _ -> not $ (Sphere c' r' surf) `containedIn` gp
