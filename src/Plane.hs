{-# LANGUAGE NamedFieldPuns #-}

module Plane
  ( Plane (..)
  , planeFromPoints
  ) where

import Linear.Metric
import Linear.V3
import Linear.Vector

import Intersection
import LinAlg
import Shapes
import Surface

data Plane = Plane
  { planeNormal  :: V3 Double
  , planeD       :: Double
  , planeSurface :: Surface
  } deriving Show

instance Solid Plane where
  intersect (Ray ro rd) pl@(Plane n d _)
    | vd > 0    = [] -- normal pointing away from ray
    | otherwise = let t = vo / vd
                  in
                    if t < 0
                        then []
                        else [Intersection t n (AnyPrimitive pl) False Nothing]
    where
      vd = n `dot` rd
      vo = -(n `dot` ro + d)

instance Primitive Plane where
  surface Plane { planeSurface } = planeSurface

planeFromPoints :: V3 Double -> V3 Double -> V3 Double -> Plane
planeFromPoints p0 p1 p2 = Plane n d def
  where
    a = p1 ^-^ p0
    b = p2 ^-^ p0
    n = normalize $ a `cross` b
    d = -(p0 `dot` n)
