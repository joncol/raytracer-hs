module Scene
  ( Scene (..)
  ) where

import AreaLight
import Camera
import Color
import PointLight
import Shapes

data Scene = Scene { scWidth         :: Int
                   , scHeight        :: Int
                   , scCamera        :: Camera
                   , scSolids        :: [AnySolid]
                   , scPointLights   :: [PointLight]
                   , scAreaLights    :: [AreaLight]
                   , scAmbientColor  :: Color
                   , scMaxLevel      :: Int
                   , scSuperSampling :: Int
                   , scShadowGridN   :: Int
                   , scMinWeight     :: Double
                   }
