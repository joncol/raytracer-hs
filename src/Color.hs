module Color where

import Codec.Picture
import Control.Monad
import Data.Word
import Numeric
import Text.Parsec

import Math

-- Integer values.
data RGB = RGB !Word8 !Word8 !Word8 deriving (Eq, Show)

-- Floating point values.
data Color = Color !Double !Double !Double deriving Show

type Parser a = Parsec String () a

type RGB8 = (Pixel8, Pixel8, Pixel8)

black :: Color
black = Color 0 0 0

white :: Color
white = Color 1 1 1

magenta :: Color
magenta = Color 1 0 1

rgbFromString :: String -> RGB
rgbFromString s = case parse parseRGB "" s of
                    Left _  -> RGB 255 0 255
                    Right c -> c

parseRGB :: Parser RGB
parseRGB = do
  optional $ char '#'
  [r, g, b] <- replicateM 3 (fst . head . readHex <$> replicateM 2 hexDigit)
  return $ RGB r g b

colorToRGB8 :: Color -> RGB8
colorToRGB8 (Color r g b) = ( scaleVal r
                            , scaleVal g
                            , scaleVal b
                            )
  where scaleVal x = clamp 0 255 $ floor $ x * 255

colorToPixel :: Color -> PixelRGB8
colorToPixel (Color r g b) = PixelRGB8 (scaleVal r)
                                       (scaleVal g)
                                       (scaleVal b)
  where scaleVal x = clamp 0 255 $ floor $ x * 255

colAdd :: Color -> Color -> Color
colAdd (Color r g b) (Color r' g' b') = Color (r + r') (g + g') (b + b')

colMul :: Color -> Color -> Color
colMul (Color r g b) (Color r' g' b') = Color (r * r') (g * g') (b * b')

colScale :: Double -> Color -> Color
colScale s (Color r g b) = Color (s * r) (s * g) (s * b)

colClamp :: Color -> Color
colClamp (Color r g b)  = Color (c r) (c g) (c b)
  where c = clamp 0 1

colorFromString :: String -> Color
colorFromString s =
  let RGB r g b = rgbFromString s
  in  Color (fromIntegral r / 255) (fromIntegral g / 255) (fromIntegral b / 255)
