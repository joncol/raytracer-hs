module Math
  ( Root (..)
  , average
  , clamp
  , degToRad
  , epsilon
  , radToDeg
  , solveQuadratic
  ) where

import Data.List (genericLength)

data Root = NoRoots
          | OneRoot !Double
          | TwoRoots !Double !Double
          | InfiniteRoots
          deriving Show

epsilon :: Double
epsilon = 1e-5

radToDeg :: Double -> Double
radToDeg x = x * 180 / pi

degToRad :: Double -> Double
degToRad x = x * pi / 180

average :: (Real a, Fractional b) => [a] -> b
average xs = realToFrac (sum xs) / genericLength xs

clamp :: Ord a => a -> a -> a -> a
{-# INLINE clamp #-}
clamp l h x
  | x <= l    = l
  | x >= h    = h
  | otherwise = x

-- | Solves equations of the form ax^2 + by + c = 0, using Viete's formula to
-- | minimize numerical instability
solveQuadratic :: Double -> Double -> Double -> Root
solveQuadratic a b c
  | a == 0 && b == 0 = if c == 0
                         then InfiniteRoots
                         else NoRoots
  | a == 0           = OneRoot $ -c / b
  | disc < 0         = NoRoots
  | disc < epsilon   = OneRoot $ -0.5 * b / a
  | otherwise        = let (x1, x2) = (q / a, c / q)
                       in  if x1 < x2
                             then TwoRoots x1 x2
                             else TwoRoots x2 x1
  where disc = b * b - 4 * a * c
        q    = -0.5 * (b + signum b * sqrt disc)
