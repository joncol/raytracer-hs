module Lighting where

import Linear.Metric
import Linear.V3
import Linear.Vector

import Color
import LinAlg

diffuseLighting :: V3 Double -> V3 Double -> V3 Double -> Color -> Color
diffuseLighting p n l col
  | d <= 0    = black
  | otherwise = d `colScale` col
  where d = diffuseFactor p n l

specularLighting :: V3 Double
                 -> V3 Double
                 -> V3 Double
                 -> Int
                 -> V3 Double
                 -> Color
                 -> Color
specularLighting p n rd shin l col
  | s <= 0    = black
  | otherwise = s `colScale` col
  where s = specularFactor p n rd shin l

diffuseFactor :: V3 Double -> V3 Double -> V3 Double -> Double
diffuseFactor p n lp = max 0 $ n `dot` l
  where
    l = normalize $ lp ^-^ p

specularFactor :: V3 Double
               -> V3 Double
               -> V3 Double
               -> Int
               -> V3 Double
               -> Double
specularFactor p n rd shin lp = max 0 $ d^shin
  where d = halfAngleDir (lp ^-^ p) (negated rd) `dot` n
