module Surface
  ( Surface (..)
  , def
  ) where

import Data.Default

import Color

data Surface = Surface
  { surfColor     :: Color
  , surfDiff      :: Double
  , surfSpec      :: Double
  , surfShininess :: Int
  , surfRefl      :: Double
  , surfTran      :: Double
  , surfRefrIndex :: Double
  } deriving Show

instance Default Surface where
  def = Surface { surfColor     = Color 1 1 1
                , surfDiff      = 1.0
                , surfSpec      = 0.5
                , surfShininess = 50
                , surfRefl      = 0
                , surfTran      = 0
                , surfRefrIndex = 1
                }
