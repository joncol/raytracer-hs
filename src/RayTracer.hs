{-# LANGUAGE BangPatterns, FlexibleContexts, NamedFieldPuns, RecordWildCards,
             TypeOperators #-}

module RayTracer
  ( render
  ) where

import           Codec.Picture
import           Control.Monad (foldM)
import           Data.Array.Repa (Array, DIM2, U, D, Z (..), (:.)(..), (!), ix2)
import qualified Data.Array.Repa as R
import           Data.Bool (bool)
import           Linear.Metric
import           Linear.V3
import           Linear.Vector
import           Data.List (sortBy)
import           Data.Map.Strict (Map)
import qualified Data.Map.Strict as M
import           Data.Ord
import           System.Console.AsciiProgress

import           AreaLight
import           Camera
import           Color
import           Intersection
import           Lighting
import           LinAlg
import           Math
import           MyScene
import           PointLight
import           Rectangle
import           Scene
import           Shapes
import           Surface

type IsectList = [Intersection AnyPrimitive]
type ChunkMap = Map (Int, Int) (Array U DIM2 RGB8)

toImage :: Int      -- ^ Chunk size (in pixels)
        -> Int      -- ^ Number of chunks in horizontal direction
        -> Int      -- ^ Number of chunks in vertical direction
        -> ChunkMap -- ^ Chunks
        -> Image PixelRGB8

toImage s w h chunks = generateImage gen (w * s) (h * s)
  where
    gen x y =
      let
        (cx, ox)  = divMod x s
        (cy, oy)  = divMod y s
        chunk     = M.lookup (cx, cy) chunks
        (r, g, b) = case chunk of
                      Nothing -> (255, 0, 255)
                      Just c  -> c ! (Z :. ox :. oy)
      in PixelRGB8 r g b

render :: Int -> Int -> Int -> IO (Image PixelRGB8)
render w h chunkSize =
  do
    pg <- newProgressBar def
      { pgFormat       = "Rendering... :percent [:bar] " ++
                         "(elapsed: :elapsed, :eta remaining)"
      , pgWidth        = 100
      , pgTotal        = fromIntegral $ xChunks * yChunks
      , pgOnCompletion = Just "Rendering took :elapsed seconds"
      }

    -- scene <- scene1 w h
    -- scene <- sphereFlakeScene w h
    scene <- simpleScene w h
    toImage chunkSize xChunks yChunks <$> foldM (go scene pg) M.empty cds

  where
    xChunks   = w `div` chunkSize
    yChunks   = h `div` chunkSize
    cds       = flip (,) <$> [0..yChunks-1] <*> [0..xChunks-1]
    go scene pg acc (cx, cy) =
      do
        img <- R.computeUnboxedP (renderChunk scene chunkSize cx cy)
        tick pg
        return $ M.insert (cx, cy) img acc

renderChunk :: Scene -> Int -> Int -> Int -> Array D DIM2 RGB8
renderChunk scene s x y =
  R.fromFunction (ix2 s s) $ renderPixel scene (s * x) (s * y)

renderPixel :: Scene -> Int -> Int -> DIM2 -> RGB8
renderPixel scene ox oy (Z :. x :. y) =
  superSamplePixel scene (ox + x) (oy + y) (scSuperSampling scene)

superSamplePixel :: Scene -> Int -> Int -> Int -> RGB8
superSamplePixel scene px py n =
  colorToRGB8 . colScale s $ foldr accumRays black grid
  where
    x         = fromIntegral px
    y         = fromIntegral py
    s         = recip . fromIntegral $ n * n
    Grid grid = regular2DGrid n x y 1 1
    accumRays (dx, dy) acc =
      colAdd acc $ trace scene 0 1 (cameraRay scene dx dy)

cameraRay :: Scene -> Double -> Double -> Ray
cameraRay scene x y = Ray cp rd
  where
    w   = scWidth scene
    h   = scHeight scene
    x'  = x / fromIntegral w -- (0..1)
    y'  = y / fromIntegral h -- (0..1)
    x'' = 2 * x' - 1 -- (-1..1)
    y'' = 1 - 2 * y' -- (-1..1)

    asp = fromIntegral w / fromIntegral h
    cam = scCamera scene
    fov = degToRad . camFov $ cam
    s   = tan (fov / 2)
    sx  = x'' * s * asp
    sy  = y'' * s
    cp  = camPos cam
    cd  = normalize . camDir $ cam
    cr  = cd `cross` (normalize . camUp $ cam)
    cu  = cr `cross` cd
    rd  = normalize (sx *^ cr ^+^ sy *^ cu ^+^ cd)

trace :: Scene -> Int -> Double -> Ray -> Color
trace scene lvl w ray@(Ray _ rd) =
  let hits = sortedIntersections scene ray
  in  case hits of
        [] -> scAmbientColor scene
        Intersection {..} : _ -> shade scene lvl w p n rd hits
          where
            p = rayPoint ray isectT
            -- n = bool id negate (isectNormal `dot` rd > 0) $ isectNormal
            n = isectNormal

sortedIntersections :: Scene -> Ray -> IsectList
{-# INLINE sortedIntersections #-}
sortedIntersections scene ray =
  sortBy (comparing isectT) $ concatMap (intersect ray) solids
  where solids = scSolids scene

shade :: Scene
      -> Int       -- ^ Recursive level
      -> Double    -- ^ Weight
      -> V3 Double -- ^ Point
      -> V3 Double -- ^ Normal
      -> V3 Double -- ^ Ray direction
      -> IsectList -- ^ List of intersections
      -> Color
{-# INLINE shade #-}
shade _ _ _ _ _ _ [] = error "Empty intersection list passed to shade"
shade scene lvl w p n rd (Intersection { isectPrim = AnyPrimitive prim } : _) =
    colClamp $ colScale kd diffCol `colMul` surfCol
      `colAdd` colScale ks specCol
      `colAdd` (colScale kr reflCol `colMul` surfCol)
      `colAdd` (colScale kt tranCol `colMul` surfCol)
      `colAdd` ambCol
  where
    surf    = surface prim
    surfCol = surfColor surf
    -- kd      = clamp 0 1 (surfDiff surf - kt)
    kd      = surfDiff surf
    ks      = surfSpec surf
    kshine  = surfShininess surf
    kr      = surfRefl surf
    kt      = surfTran surf
    pls     = visLights $ scPointLights scene
    genPls  = areaLightsToPointLights scene p
    lights  = pls ++ genPls

    diffCol = accLighting (diffuseLighting p n) lights
    specCol = accLighting (specularLighting p n rd kshine) lights

    minW    = scMinWeight scene
    maxLvl  = scMaxLevel scene

    reflCol
      | lvl+1 < maxLvl && w*kr > minW = trace scene (lvl+1) (w*kr) reflRay
      | otherwise                     = black

    tranCol
      | lvl+1 < maxLvl && w*kt > minW =
          case tranDir of
            Nothing -> black -- TODO: Total internal reflection
            Just td -> trace scene (lvl+1) (w*kr) $ rayOffset (Ray p td) epsilon
      | otherwise = black

    visLights = filter (\l -> (>0) $ shadowFactor scene p (plPos l))
    reflRay   = rayOffset (Ray p $ reflDirection rd n) epsilon
    tranDir   = tranDirection relRefr rd n
    relRefr   = 1.1
    ambCol    = scAmbientColor scene

accLighting :: (V3 Double -> Color -> Color) -> [PointLight] -> Color
{-# INLINE accLighting #-}
accLighting f = colClamp . foldr (aux f) black
  where aux g l acc = acc `colAdd` g (plPos l) (plColor l)

areaLightsToPointLights :: Scene -> V3 Double -> [PointLight]
{-# INLINE areaLightsToPointLights #-}
areaLightsToPointLights scene p =
  concatMap (areaLightToPointLights scene p) als
  where als = scAreaLights scene

areaLightToPointLights :: Scene -> V3 Double ->AreaLight -> [PointLight]
{-# INLINE areaLightToPointLights #-}
areaLightToPointLights scene p l =
  map (createPointLight scene p col n) $ areaLightPoints l
  where
    n   = scShadowGridN scene
    col = surfColor . rectSurface . areaLightRect $ l

areaLightPoints :: AreaLight -> [V3 Double]
{-# INLINE areaLightPoints #-}
areaLightPoints (AreaLight l (Grid grid)) =
  map f grid
  where
    c         = rectCenter l
    r         = rectRight l
    u         = rectUp l
    f (x, y)  = x *^ r ^+^ y *^ u ^+^ c

createPointLight :: Scene -> V3 Double -> Color -> Int -> V3 Double -> PointLight
{-# INLINE createPointLight #-}
createPointLight scene p col n lp =
  PointLight lp (colScale (sf / fromIntegral (n * n)) col)
  where
    sf = shadowFactor scene p lp

-- | Calculate shadow factor for surface on point.
shadowFactor :: Scene
             -> V3 Double -- ^ The point on the surface.
             -> V3 Double -- ^ The light source position.
             -> Double    -- ^ 1 if the point is lit, 0 if not lit.
{-# INLINE shadowFactor #-}
shadowFactor scene p l =
  case hits of
    []                              -> 1
    Intersection { isectT = t } : _ -> bool 1 0 (t * t < tmaxsq)
  where
    hits   = sortedIntersections scene lray
    pl     = l ^-^ p
    lray   = rayOffset (Ray p $ normalize pl) epsilon
    tmaxsq = pl `dot` pl
