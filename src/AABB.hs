{-# LANGUAGE NamedFieldPuns, RecordWildCards #-}

module AABB where

import           Control.Lens
-- import           Data.Semigroup
-- import qualified Data.Vector as V
-- import           Linear.Metric
import           Linear.V3
-- import           Linear.Vector

import           Intersection
import           LinAlg
import           Math (epsilon)
import           Shapes
import           Surface

data AABB = AABB { aabbP1 :: V3 Double
                 , aabbP2 :: V3 Double
                 , aabbSurface :: Surface
                 } deriving Show

instance Solid AABB where
  -- Assumes direction of ray is normalized.
  intersect (Ray o d) aabb@(AABB p1 p2 surf) | or $ map not [xOk, yOk, zOk] = []
    | t1x' > t2y' || t1y' > t2x' = []
    | t1x' > t2z' || t1z' > t2x' = []
    | tNear > tFar || tFar < 0   = []
    | tNear < 0 = [Intersection tFar nFar prim False (Just surf)]
    | otherwise = [ Intersection tNear nNear prim True Nothing
                  , Intersection tFar nFar prim False (Just surf)]

    where minPoint = V3 (min (p1 ^. _x) (p2 ^. _x))
                        (min (p1 ^. _y) (p2 ^. _y))
                        (min (p1 ^. _z) (p2 ^. _z))
          maxPoint = V3 (max (p1 ^. _x) (p2 ^. _x))
                        (max (p1 ^. _y) (p2 ^. _y))
                        (max (p1 ^. _z) (p2 ^. _z))

          nox = V3 (signum d ^. _x) 0 0
          noy = V3 0 (signum d ^. _y) 0
          noz = V3 0 0 (signum d ^. _z)
          nix = negate nox
          niy = negate noy
          niz = negate noz

          xOk = abs (d ^. _x) > epsilon ||
                (minPoint ^. _x < (o ^. _x) && (o ^. _x) < maxPoint ^. _x)

          yOk = abs (d ^. _y) > epsilon ||
                (minPoint ^. _y < (o ^. _y) && (o ^. _y) < maxPoint ^. _y)

          zOk = abs (d ^. _z) > epsilon ||
                (minPoint ^. _z < (o ^. _z) && (o ^. _z) < maxPoint ^. _z)

          t1x  = (minPoint ^. _x - o ^. _x) / d ^. _x
          t2x  = (maxPoint ^. _x - o ^. _x) / d ^. _x
          t1x' = min t1x t2x
          t2x' = max t1x t2x

          t1y  = (minPoint ^. _y - o ^. _y) / d ^. _y
          t2y  = (maxPoint ^. _y - o ^. _y) / d ^. _y
          t1y' = min t1y t2y
          t2y' = max t1y t2y

          t1z  = (minPoint ^. _z - o ^. _z) / d ^. _z
          t2z  = (maxPoint ^. _z - o ^. _z) / d ^. _z
          t1z' = min t1z t2z
          t2z' = max t1z t2z

          (tNear, nNear)
            | t1x' > t1y' && t1x' > t1z' = (t1x', nix)
            | t1y' > t1x' && t1y' > t1z' = (t1y', niy)
            | otherwise                  = (t1z', niz)

          (tFar, nFar)
            | t2x' < t2y' && t2x' < t2z' = (t2x', nox)
            | t2y' < t2x' && t2y' < t2z' = (t2y', noy)
            | otherwise                  = (t2z', noz)

          prim  = AnyPrimitive aabb

instance Primitive AABB where
  surface AABB {..} = aabbSurface
