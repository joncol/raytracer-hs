module PointLight where

import Linear.V3

import Color

data PointLight = PointLight
  { plPos   :: V3 Double
  , plColor :: Color
  }
