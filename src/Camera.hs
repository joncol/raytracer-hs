module Camera where

import Linear.Metric
import Linear.V3
import Linear.Vector

data Camera = Camera
  { camPos :: V3 Double
  , camDir :: V3 Double
  , camUp  :: V3 Double
  , camFov :: Double
  } deriving Show

cameraLookAt :: V3 Double -> V3 Double -> Camera
cameraLookAt p l = Camera p d u 60
  where
    d = normalize (l ^-^ p)
    u = V3 0 1 0
