module Main where

import Codec.Picture
import System.Console.AsciiProgress
import System.IO

import RayTracer

main :: IO ()
main = displayConsoleRegions $ do
  hSetBuffering stdout NoBuffering

  -- render 40 30 10 >>= writePng "output.png"
  -- render 320 192 64 >>= writePng "output.png"
  -- render 512 384 32 >>= writePng "output.png"
  render 512 384 128 >>= writePng "output.png"
  -- render 1024 768 8 >>= writePng "output.png"
